Grrla is a text-mode [Guerrilla Mail][gm] client with a [mutt][]-like
user interface, written in [Go][]. It is named after one of Guerrilla
Mail's most notorious domain names.

[gm]:		https://www.guerrillamail.com/
[mutt]:	http://www.mutt.org/
[go]:		https://golang.org/

# Features #

- Obtain temporary addresses
- Receive e-mail
- Delete e-mail
- Set custom username and domain
- Save messages to local files
- Highlight URLs in e-mail body
- Run custom command on e-mail address, alias address and highlighted URL
	- suggested uses: copying to clipboard, launching a web browser
- Auto-detect clipboard command in some cases

# Obtaining it #

Once you've [set up go](https://golang.org/doc/install), this should do it:

	go get -u gitlab.com/lonvale/grrla

Grrla depends on the following packages:

- [termbox-go](https://github.com/nsf/termbox-go)
- [go-runewidth](https://github.com/mattn/go-runewidth)
- [sharklaser](https://gitlab.com/lonvale/sharklaser)

# Using it #

Grrla does not take any command-line arguments. It does read options
from a configuration file if it can find it. Currently, configuration
file location is not cross-platform and will only work in some major
GNU/Linux distributions and maybe other Unix-likes - work on that is
coming up.

Once running, grrla is controlled through key bindings, which are
displayed on screen. The help '?' command, where available, provides
more details.

# License #

Grrla is [Free Software][freesw] made available under the terms of the
GNU General Public License, version 3 or later.

[freesw]: https://www.gnu.org/philosophy/free-sw.html
