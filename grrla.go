/* Copyright 2018 grrla authors

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

/*
Grrla is a text-mode client for the Guerrilla Mail API with a mutt-like
interface. Its name is a reference to a domain name famously owned by
Guerrilla Mail.

Grrla can do the things you'd expect it to: obtain a temporary e-mail
address, receive e-mails, delete e-mails, set a custom username and
domain name and extend or renew the address' lifetime.

It also has some additional features, like saving messages to disk,
highlighting URLs in messages, and running a custom command with the
e-mail address, alias address or highlighted URL as a parameter. This
can be used to copy the address to the clipboard (grrla will try to
detect a clipboard command and use it as default) or launch a web
browser.

It currently does not take any command-line arguments. It does read
the following options from a configuration file:

	- address_cmd		Command to run with e-mail address
	- alias_cmd		Command to run with alias address
	- link_cmd		Command to run with highlighted URL
	- auto_getaddr Whether to request an address on program start
	- auto_extend		Whether to extend address lifetime automatically
	- auto_renew		Whether to renew an expired address automatically
	- get_mail_on_start	Whether to get e-mail automatically on program start
	- subscr_cookie		Guerrilla Mail subscription cookie
	- email_user		Custom username to request
	- domain_name		Preferred domain name

The configuration file is called grrlarc and should be looked for in a
number of plausible locations in a multiplatform manner, but currently
isn't.

*/
package main

import (
	"bufio"
	"errors"
	"fmt"
	"github.com/mattn/go-runewidth"
	"github.com/nsf/termbox-go"
	"gitlab.com/lonvale/sharklaser"
	"os"
	"os/exec"
	"regexp"
	"runtime"
	"strings"
	"time"
)

// Misc constants.
const (
	GRRLA_VERSION        = "1.1.0"
	USERAGENT            = "grrla " + GRRLA_VERSION
	EXTEND_THRESHOLD     = int64(300)
	EXTEND_INTERVAL      = int64(60) // To avoid harassing the server/my code going berserk.
	CMD_ARG_PLACEHOLDER  = "%s"
	REGEXP_URL_EXPR      = `http(s)?://([A-Za-z0-9]+\.)+[A-Za-z]+([A-Za-z0-9_\-~%!\+\./]+[A-Za-z0-9_\-~%!\+\?=&#]+)?`
	REGEXP_PRINTABLE     = `[[:print:]]`
	USERNAME_INPUT_WIDTH = 20
)

var REGEXP_URL = regexp.MustCompile(REGEXP_URL_EXPR)

// Termbox attribute constants.
const (
	TEXTBG     = termbox.ColorBlack // Mutt colours.
	TEXTFG     = termbox.ColorWhite
	HEADBG     = termbox.ColorBlue
	HEADFG     = termbox.ColorGreen
	ERRORBG    = termbox.ColorBlack
	ERRORFG    = termbox.ColorRed | BOLD
	SELMSGBG   = termbox.ColorCyan // Selected message in list.
	SELMSGFG   = termbox.ColorBlack
	LINKBG     = TEXTBG // URLs in message body.
	LINKFG     = termbox.ColorGreen | UNDERLINE
	SELLINKBG  = TEXTBG // 'Selected' URL.
	SELLINKFG  = termbox.ColorYellow | BOLD | UNDERLINE
	INPUTBOXBG = termbox.ColorYellow
	INPUTBOXFG = termbox.ColorBlack
	BOLD       = termbox.AttrBold
	UNDERLINE  = termbox.AttrUnderline
	REVERSE    = termbox.AttrReverse
)

// Status and error messages, UI text.
const (
	STATUS_CHECK_EMAIL   = "Checking for new email"
	STATUS_EXTEND        = "Requesting address lifetime extension."
	STATUS_RENEW         = "Requesting renewal of expired address."
	STATUS_SETUSR        = "New username requested."
	STATUS_CMD_ADDR      = "Address command successful."
	STATUS_CMD_ALIAS     = "Alias command successful."
	STATUS_CMD_URL       = "URL command successful."
	STATUS_SAVE_MSG      = "Email saved."
	STATUS_MAX_AGE       = 5
	ERROR_CMD_EMPTY      = "No command specified."
	ERROR_STRING_EMPTY   = "String is empty."
	ERROR_NO_CONF_FILE   = "Config file not found"
	LABEL_USERNAME_INPUT = "Set username"
	LABEL_ENTER          = "Enter"
	LABEL_ESCAPE         = "Escape"
)

// UI modes of operation.
const (
	MODE_IDLE = iota
	MODE_INBOX
	MODE_MESSAGE
	MODE_HELP
	MODE_SETEMAIL
)

// Headers to print under each of the modes above.
var mode_headers = [][][2]string{
	{{"q:Quit", "Exit grrla"},
		{"g:Get address", "Request a temporary email address"},
		{"?:Help", "Show this message"}},
	{{"Q:Quit", "Discard this address and exit grrla"},
		{"c:Address cmd", "Run address_cmd on this address"},
		{"r:Check email", "Check for new incoming emails"},
		{"?:Help", "Show this message"},
		{"s:Set email", "Set a custom email address"},
		{"d:Flag", "Flag email for deletion"},
		{"D:Delete", "Delete flagged emails"},
		{"C:Alias cmd", "Run alias_cmd on this address' alias"},
		{"F:Forget me", "Discard this email address"}},
	{{"q:Back", "Go back to inbox"},
		{"s:Save", "Save message to a local file"},
		{"c:URL cmd", "Run link_cmd on this URL"},
		{"?:Help", "Show this message"},
		{"d:Flag", "Flag email for deletion"}},
	{{"q:Back", "Go back"}},
	{{"<Enter>:Set user", ""},
		{"<Escape>:Cancel", ""},
		{"<Tab>:Switch domain", ""},
		{"<Backspace>:Delete character", ""}},
}

type Options struct {
	address_cmd       string
	alias_cmd         string
	link_cmd          string
	auto_getaddr      bool
	auto_extend       bool
	auto_renew        bool
	get_mail_on_start bool
	subscr_cookie     string
	email_user        string
	domain_name       string
}

// Grrla contains all variables and methods related to drawing the UI,
// plus the program's options and a Sharklaser instance.
type Grrla struct {
	options                *Options
	*sharklaser.Sharklaser     // Embed in this namespace.
	mode                   int // Mode.
	previous_mode          int
	width                  int // Screen size.
	height                 int
	ypos_after_header      int // UI widget positions and sizes.
	ypos_footer            int
	ypos_status            int
	height_inbox_list      int
	height_msg_display     int
	width_setusr_box       int
	xpos_setusr_box        int
	inbox_sel_msg          int    // Inbox list view: selected message within page
	inbox_page_offset      int    // and list offset for current page.
	msg_current            int    // Single message view: selected message.
	paging_text            string // Current text to page: email body or help string.
	paging_linebreaks      []int  // Linebreak offsets for paging text.
	paging_line_offset     int
	msg_link_indices       [][]int
	msg_selected_link      int
	setusr_username        string
	setusr_domain_index    int
	status_message         string
	err                    error
	status_latest_time     int64
}

// switch_mode sets the current UI mode and sets mode variables as appropriate.
func (grrla *Grrla) switch_mode(mode int) {
	grrla.previous_mode = grrla.mode
	grrla.mode = mode
	// Variables cleared on mode change.
	grrla.msg_selected_link = 0
	termbox.HideCursor()
	// Mode hooks go here.
	if grrla.mode == MODE_IDLE {
		grrla.prepare_paging("")
	} else if grrla.mode == MODE_INBOX {
		if grrla.previous_mode == MODE_IDLE {
			grrla.setusr_domain_index = get_domain_name_index(grrla.options.domain_name, grrla.EmailAddr)
		}
	} else if grrla.mode == MODE_MESSAGE {
		grrla.prepare_paging(grrla.Emails[grrla.msg_current].Mail_body)
		grrla.msg_link_indices = REGEXP_URL.FindAllStringIndex(grrla.paging_text, -1)
	} else if grrla.mode == MODE_HELP {
		grrla.msg_link_indices = [][]int{}
		grrla.prepare_paging(generate_help_text(grrla.previous_mode, grrla.options))
	}
}

// resize stores current width and height and calculates postional
// variables for UI widgets based on them.
// Run this method after termbox.Sync() to re-calculate UI layout.
func (grrla *Grrla) resize(width, height int) {
	grrla.width = width
	grrla.height = height
	lines_available := height - 3 // Header + footer + status.
	grrla.ypos_after_header = 1
	grrla.ypos_footer = height - 2
	grrla.ypos_status = height - 1
	grrla.height_inbox_list = lines_available / 2
	grrla.height_msg_display = lines_available
	grrla.width_setusr_box = USERNAME_INPUT_WIDTH + 6
	grrla.xpos_setusr_box = (width - grrla.width_setusr_box) / 2
}

func (grrla *Grrla) set_error(err error) {
	grrla.err = err
	grrla.status_latest_time = time.Now().Unix()
}

func (grrla *Grrla) set_status(status string) {
	grrla.status_message = status
	grrla.status_latest_time = time.Now().Unix()
}

// draw draws the user interface.
func (grrla *Grrla) draw() {
	termbox.Clear(TEXTFG, TEXTBG)

	// Erase status messages after a while.
	if grrla.status_latest_time+STATUS_MAX_AGE <= time.Now().Unix() {
		grrla.status_message = ""
	}

	// UI elements for all modes.
	print_header(grrla.width, grrla.mode)
	print_footer(grrla.ypos_footer, grrla.width, grrla.EmailAddr, grrla.update_in())
	if grrla.err != nil {
		print_status(grrla.ypos_status, grrla.width, grrla.err.Error(), true)
	} else if grrla.status_message != "" {
		print_status(grrla.ypos_status, grrla.width, grrla.status_message, false)
	}

	if grrla.mode == MODE_INBOX && len(grrla.Emails) > grrla.inbox_page_offset {
		print_email_list(grrla.ypos_after_header, grrla.width, grrla.height_inbox_list, grrla.Emails[grrla.inbox_page_offset:], grrla.inbox_sel_msg, grrla.inbox_page_offset)
		grrla.prepare_paging(grrla.Emails[grrla.msg_current].Mail_body)
	}
	grrla.draw_text() // Prints each mode's main text element.
	// Username input box is drawn on top of inbox view.
	if grrla.mode == MODE_SETEMAIL {
		print_input_box(grrla.xpos_setusr_box, grrla.ypos_after_header, grrla.width_setusr_box, grrla.setusr_username, grrla.setusr_domain_index, LABEL_USERNAME_INPUT, USERNAME_INPUT_WIDTH)
	}

	termbox.Flush()
}

// update_in returns a string with the text "Update in: " plus a pretty
// representation of the time left to the next automatic check for email.
// If no session is active, it returns an empty string.
func (grrla *Grrla) update_in() string {
	// Pretty time left to next automatic email update.
	if !grrla.AddrActive {
		return ""
	}
	secs := grrla.NextUpdate - time.Now().Unix()
	if secs < 0 {
		secs = 0
	}
	mins := 0
	for secs >= 60 { // Convert to minutes:seconds.
		mins++
		secs -= 60
	}
	next_update_string := "Update in: "
	if mins > 0 {
		next_update_string += fmt.Sprintf("%d:", mins)
	}
	next_update_string += fmt.Sprintf("%02d", secs)
	return next_update_string
}

// prepare_paging stores a text into grrla.paging_text and calculates
// linebreak offsets. Run it whenever the main widget's text changes
// (i.e. on mode switch or text scrolling).
func (grrla *Grrla) prepare_paging(text string) {
	grrla.paging_text = text
	grrla.paging_linebreaks = find_linebreaks(grrla.paging_text, grrla.width)
	grrla.paging_line_offset = 0
}

// draw_text prints the text that has been prepared for paging, with different options depending on mode.
func (grrla *Grrla) draw_text() {
	// Defaults: take up all available screen, do not highlight URLs.
	ypos_start := grrla.ypos_after_header
	highlight_links := false
	if grrla.mode == MODE_INBOX { // Print email body in bottom half.
		ypos_start = grrla.height_inbox_list + grrla.ypos_after_header
	} else if grrla.mode == MODE_MESSAGE {
		highlight_links = true // Enable URL highlighting.
	}
	print_paged_text(ypos_start, grrla.ypos_footer, grrla.paging_text, grrla.paging_linebreaks, grrla.paging_line_offset, highlight_links, grrla.msg_link_indices, grrla.msg_selected_link)
}

// scroll scrolls current paging text by the amount appropriate for
// each key press, taking care to keep line offset within bounds.
func (grrla *Grrla) scroll(key termbox.Key) {
	grrla.paging_line_offset += map_key_to_scroll(key, grrla.height_msg_display, len(grrla.paging_linebreaks))
	if grrla.paging_line_offset+grrla.height_msg_display > len(grrla.paging_linebreaks) {
		grrla.paging_line_offset = len(grrla.paging_linebreaks) - grrla.height_msg_display
	} // Don't make this an else/if! The above case can make it go below 0!
	if grrla.paging_line_offset < 0 {
		grrla.paging_line_offset = 0
	}
}

// scroll_message_list calculates the list offset and message index
// needed to scroll the selected message by a certain amount. It takes
// care to keep selection within the bounds of the list display height
// and actual message list. With an amount of 0, it can be used to
// recalculate inbox view when screen size or email count change.
func (grrla *Grrla) scroll_message_list(amount, message_count int) {
	grrla.inbox_sel_msg = grrla.inbox_page_offset + grrla.inbox_sel_msg + amount
	grrla.inbox_page_offset = 0
	if grrla.inbox_sel_msg >= message_count {
		grrla.inbox_sel_msg = message_count - 1
	} else if grrla.inbox_sel_msg <= 0 {
		grrla.inbox_sel_msg = 0
	}
	for grrla.inbox_sel_msg >= grrla.height_inbox_list {
		grrla.inbox_page_offset += grrla.height_inbox_list
		grrla.inbox_sel_msg -= grrla.height_inbox_list
	}
	grrla.msg_current = grrla.inbox_sel_msg + grrla.inbox_page_offset
}

// quit strives to exit the program gracefully.
// Most importantly, it issues Sharklaser.SLForgetMe() to allow the
// server to discard the email address.
func (grrla *Grrla) quit() {
	if grrla.AddrActive {
		grrla.SLForgetMe()
	}
	termbox.Close()
	os.Exit(0)
}

// Configuration file-related functions.

// find_config_file tries to do exactly that. Cross-platform
// considerations about appropriate configuration file location belong
// here.
func find_config_file() (filename string, our_err error) {
	// Currently this does only the half-assedest job possible.
	// Actual attempt at cross-platformness later maybe?
	var candidate string
	var err error
	switch runtime.GOOS {
	case "linux":
		candidate = os.ExpandEnv("${HOME}/.config/grrlarc")
		if _, err = os.Stat(candidate); err == nil {
			filename = candidate
			return
		}
	}
	our_err = errors.New(ERROR_NO_CONF_FILE)
	return
}

// strip_value removes trailing comments and spaces from a
// configuration file value.
func strip_value(raw string) (clean string) {
	clean = raw
	if strings.Contains(clean, "#") {
		clean = clean[:strings.Index(clean, "#")]
	}
	for rune(clean[len(clean)-1]) == ' ' {
		clean = clean[:len(clean)-1]
	}
	return
}

// parse_boolean applies simple patterns to decode a string into a
// Boolean value. In case of doubt it returns the provided default.
func parse_boolean(raw_value string, default_value bool) (value bool) {
	value = default_value
	switch strings.ToLower(raw_value) {
	case "t", "true", "on", "1", "yes":
		value = true
	case "f", "false", "off", "0", "no":
		value = false
	}
	return
}

// get_default_clipboard_cmd tries to find a command for copying a
// string to the clipboard among a few plausible candidates. The
// result will serve as the default value for the address_cmd option.
func get_default_clipboard_cmd() (cmd_s string) {
	var path string
	var err error
	// I've only tested xsel. Other ideas taken from
	// https://jonathanmh.com/golang-clipboard-mdclip/
	if path, err = exec.LookPath("xsel"); err == nil {
		// If running xsel, do remember to add the -i flag.
		// Otherwise it thinks we're a tty and does not read input from us.
		cmd_s = path + " -ib"
	} else if path, err = exec.LookPath("xclip"); err == nil {
		cmd_s = path + " -selection c"
	} else if path, err = exec.LookPath("pbcopy"); err == nil {
		// This is for Mac OS I believe.
		cmd_s = path
	}
	return
}

// parse_config_file provides the program's options. First it creates
// an Options object with default values; then it tries to find a
// configuration file and override defaults with values found within.
func parse_config_file() (options *Options, err error) {
	// A map is a convenient way to match option names against a valid
	// list and store their values. We take this chance to fill in some
	// default values now so they can be overridden. The default value
	// for alias_cmd is dynamic, however, so we fill that in later.
	options_map := map[string]string{
		"address_cmd":       get_default_clipboard_cmd(),
		"alias_cmd":         "",
		"link_cmd":          "",
		"auto_getaddr":      "",
		"auto_extend":       "",
		"auto_renew":        "",
		"get_mail_on_start": "",
		"subscr_cookie":     "",
		"email_user":        "",
		"domain_name":       ""}
	// Find and load configuration file.
	// Nested code instead of the usual if err != nil { return } thing,
	// because if we skip initialising options we'll cause a nil pointer dereference.
	fname, err := find_config_file()
	if err == nil {
		file, err := os.Open(fname)
		defer file.Close()
		if err == nil {
			scanner := bufio.NewScanner(file)
			for {
				success := scanner.Scan()
				if success == false {
					err = scanner.Err()
					break
				}
				line := scanner.Text()
				// Skip empty lines and comments.
				if len(line) > 0 && !strings.HasPrefix(line, "#") {
					option_name := strings.Split(line, " ")[0]
					// Store stripped values.
					if _, exists := options_map[option_name]; exists {
						options_map[option_name] = strip_value(strings.SplitN(line, " ", 2)[1])
					}
				}
			}
		}
	}
	// Default for alias_cmd is same as (possibly user-provided) address_cmd.
	if options_map["alias_cmd"] == "" {
		options_map["alias_cmd"] = options_map["address_cmd"]
	}
	// Fill in struct with values from map, also provide defaults for Booleans.
	options = &Options{
		address_cmd:       options_map["address_cmd"],
		alias_cmd:         options_map["alias_cmd"],
		link_cmd:          options_map["link_cmd"],
		auto_getaddr:      parse_boolean(options_map["auto_getaddr"], true),
		auto_extend:       parse_boolean(options_map["auto_extend"], true),
		auto_renew:        parse_boolean(options_map["auto_renew"], true),
		get_mail_on_start: parse_boolean(options_map["get_mail_on_start"], true),
		subscr_cookie:     options_map["subscr_cookie"],
		email_user:        options_map["email_user"],
		domain_name:       options_map["domain_name"],
	}
	return
}

// Misc functions.

// get_domain_name_index returns the position at which a domain name
// exists in sharklaser.DomainNames. It tries both the domain name in
// the current active address and a user-provided value, preferring
// the first. Since its purpose is only to pick the most fitting
// default to print and not to do any sort of validation, it is fine
// to return an index of 0 by default and no error.
func get_domain_name_index(candidate, email_address string) (index int) {
	for i, s := range sharklaser.DomainNames {
		if s == strings.Split(email_address, "@")[1] {
			return i
		} else if s == candidate {
			return i
		}
	}
	return
}

// command_with_string runs a command and passes it a string. It can
// be used to copy a string to the clipboard or launch a web browser,
// for example. If the command contains CMD_ARG_PLACEHOLDER, it will
// be replaced with s. Otherwise, s will be sent via standard input.
func command_with_string(s, cmd_s string) (err error) {
	var cmd *exec.Cmd
	if len(cmd_s) == 0 {
		err = errors.New(ERROR_CMD_EMPTY)
		return
	} else if len(s) == 0 {
		err = errors.New(ERROR_STRING_EMPTY)
		return
	}
	cmd_split := strings.Split(cmd_s, " ")
	// If command string contains CMD_ARG_PLACEHOLDER, replace with
	// target string and run (for commands that take input as a
	// command-line argument).
	if strings.Contains(cmd_s, CMD_ARG_PLACEHOLDER) {
		// First split and then replace pattern with target string.
		// That way, if target string contains spaces it won't be split.
		for i, _ := range cmd_split {
			cmd_split[i] = strings.Replace(cmd_split[i], CMD_ARG_PLACEHOLDER, s, -1)
		}
		cmd = exec.Command(cmd_split[0], cmd_split[1:]...)
		if err = cmd.Start(); err != nil {
			return
		}
		// Else, run and send target string via standard input.
	} else {
		cmd = exec.Command(cmd_split[0], cmd_split[1:]...)
		cmd_stdin, err := cmd.StdinPipe()
		if err != nil {
			return err
		}
		if err = cmd.Start(); err != nil {
			return err
		}
		if _, err = cmd_stdin.Write([]byte(s)); err != nil {
			return err
		}
		cmd_stdin.Close()
	}
	return
}

// save_message saves an email to a local file.
// The file's name is composed of current email address and the
// message's numeric ID.
func save_message(address string, email sharklaser.Email) (err error) {
	outfname := address + "_" + email.Mail_id
	outfile, err := os.Create(outfname)
	defer outfile.Close()
	if err != nil {
		return
	}
	_, err = outfile.WriteString(email.Mail_body)
	return
}

// Misc UI-related utility functions.

// map_key_to_scroll maps a key event to a number of lines to scroll.
// It exists to allow shorter, more readable code in the keyboard
// event reaction section. This way, a single case statement fits all
// cases.
func map_key_to_scroll(key termbox.Key, display_height, total_lines int) int {
	switch key {
	case termbox.KeyArrowUp:
		return -1
	case termbox.KeyArrowDown:
		return 1
	case termbox.KeyPgup:
		return -display_height
	case termbox.KeyPgdn:
		return display_height
	case termbox.KeyHome:
		return -total_lines
	case termbox.KeyEnd:
		return total_lines
	default:
		return 0
	}
}

// find_linebreaks simulates pretty-printing a string with a given
// screen width to find the offsets at which it should be split into
// lines.
func find_linebreaks(text string, width int) (linebreaks []int) {
	linebreaks = []int{0}
	linepos := 0
	for pos, c := range text {
		if linepos >= width { // Not == in case +=runewidth causes it to go 79->81.
			linebreaks = append(linebreaks, pos)
			linepos = 0
		} else if c == '\n' {
			linebreaks = append(linebreaks, pos+1)
			linepos = -1
		}
		linepos += runewidth.RuneWidth(c)
	}
	return
}

// generate_help_text produces a multi-line string containing the key
// bindings and descriptions for a given mode plus the names and
// current values of the program's options.
func generate_help_text(mode int, options *Options) (text string) {
	for _, entry := range mode_headers[mode] {
		text += "  " + entry[0] + "\n"
		text += "    " + entry[1] + "\n"
	}
	text += "\n"
	text += "  Options:\n"
	text += "    " + fmt.Sprintf("%-19s", "address_cmd:")
	text += options.address_cmd + "\n"
	text += "    " + fmt.Sprintf("%-19s", "alias_cmd:")
	text += options.alias_cmd + "\n"
	text += "    " + fmt.Sprintf("%-19s", "link_cmd:")
	text += options.link_cmd + "\n"
	text += "    " + fmt.Sprintf("%-19s", "auto_getaddr:")
	text += fmt.Sprintf("%t", options.auto_getaddr) + "\n"
	text += "    " + fmt.Sprintf("%-19s", "auto_extend:")
	text += fmt.Sprintf("%t", options.auto_extend) + "\n"
	text += "    " + fmt.Sprintf("%-19s", "auto_renew:")
	text += fmt.Sprintf("%t", options.auto_renew) + "\n"
	text += "    " + fmt.Sprintf("%-19s", "get_mail_on_start:")
	text += fmt.Sprintf("%t", options.get_mail_on_start) + "\n"
	text += "    " + fmt.Sprintf("%-19s", "subscr_cookie:")
	text += options.subscr_cookie + "\n"
	text += "    " + fmt.Sprintf("%-19s", "email_user:")
	text += options.email_user + "\n"
	text += "    " + fmt.Sprintf("%-19s", "domain_name:")
	text += options.domain_name + "\n"
	text += "\n"
	text += "  " + USERAGENT
	return
}

// tb_getevent sends termbox events through a channel in an infinite loop.
// Ran as a goroutine, it provides a convenient way to react to events
// non-blockingly with a select statement.
func tb_getevent(channel chan termbox.Event) {
	for {
		channel <- termbox.PollEvent()
	}
}

// Termbox drawing wrapper functions.
// Arguments try to follow this order:
// X start, Y start, X max, Y max, contents, other.

// tb_print prints as much of text as fits in the rectangle between
// the provided endpoints, ignoring line breaks and such.
func tb_print(xstart, ystart, xmax, ymax int, text string, fg, bg termbox.Attribute) {
	x, y := xstart, ystart
	for _, c := range text {
		w := runewidth.RuneWidth(c)
		if x+w > xmax {
			x = xstart
			y++
			if y > ymax {
				return
			}
			termbox.SetCell(x, y, c, fg, bg)
			x += w
		} else {
			termbox.SetCell(x, y, c, fg, bg)
			x += w
		}
		if x >= xmax {
			x = xstart
			y++
			if y > ymax {
				return
			}
		}
	}
}

// fill_line fills a line with a background colour.
// Running it before printing text on the same line is an easy way to
// ensure no default-coloured gaps are left.
func fill_line(xstart, y, xmax int, fg, bg termbox.Attribute) {
	for i := xstart; i < xmax; i++ {
		termbox.SetCell(i, y, '\x00', fg, bg)
	}
}

// print_header prints the current mode's header
// It will print as many entries as will fit whole and stop. Header
// may contain the most important keyboard bindings for each mode.
func print_header(width, mode int) {
	xpos := 0
	fill_line(0, 0, width, HEADFG, HEADBG)
	for _, entry := range mode_headers[mode] {
		entry_width := runewidth.StringWidth(entry[0])
		if xpos+entry_width <= width {
			tb_print(xpos, 0, width, 1, entry[0], HEADFG|BOLD, HEADBG)
			xpos += entry_width + 1 // Put a space between entries.
		} else {
			break
		}
	}
}

// print_footer prints the current email address, left-justified, and
// the time left before the next automatic update, right-justified.
func print_footer(ypos, width int, head, tail string) {
	xpos := 0
	tail_length := runewidth.StringWidth(tail)
	fill_line(0, ypos, width, HEADFG, HEADBG)
	tb_print(xpos, ypos, width-tail_length-1, ypos, head, HEADFG|BOLD, HEADBG)
	// xpos += len(grrla.Email_addr)
	tb_print(width-tail_length, ypos, width, ypos, tail, HEADFG|BOLD, HEADBG)
}

// print_status prints a status message, with different styling
// depending on whether it's supposed to be an error message.
func print_status(ypos, width int, message string, is_error bool) {
	var fg, bg = TEXTFG, TEXTBG
	if is_error {
		fg, bg = ERRORFG, ERRORBG
	}
	tb_print(0, ypos, width, ypos, message, fg, bg)
}

// print_email_list prints as much of the email list provided as will
// fit in the given space, with different styling for the line
// containing the currently-selected email.
func print_email_list(start_height, width, max_height int, list []sharklaser.Email, selected, offset int) {
	var fg, bg termbox.Attribute
	var firstchar rune
	// Calculate space available for variable-width fields.
	// Subtract space needed by fixed-width fields:
	// 1 for selected email marker
	// 3 for email id #
	// 4 for new, flagged for deletion markers + surrounding spaces
	width_senderaddr := (width - 8) / 2 // Taking advantage of rounded-down division to give subject field the extra character if odd.
	// TODO: check for not enough width available.
	for i := 0; i < max_height; i++ {
		if i == len(list) {
			break
		}
		xpos := 0
		ypos := start_height + i
		if i == selected {
			fg, bg = SELMSGFG, SELMSGBG
			firstchar = '>'
		} else {
			fg, bg = TEXTFG, TEXTBG
			firstchar = ' '
		}
		// Fill with background colour.
		fill_line(0, ypos, width, fg, bg)
		email := list[i]
		// Selected email marker.
		termbox.SetCell(xpos, ypos, firstchar, fg, bg)
		xpos++
		// Email id number.
		tb_print(xpos, ypos, xpos+3, ypos, fmt.Sprintf("%3d", i+offset), fg, bg)
		xpos += 4 // Blank space before next field.
		// Flags (unread, flagged for deletion)
		if email.Mail_read == "0" {
			termbox.SetCell(xpos, ypos, 'N', fg, bg)
		}
		xpos++
		if email.FlaggedForDeletion {
			termbox.SetCell(xpos, ypos, 'D', fg, bg)
		}
		xpos += 2 // Blank space before next field.
		// Variable-width fields: sender address and subject.
		tb_print(xpos, ypos, xpos+width_senderaddr, ypos, email.Mail_from, fg, bg)
		xpos += width_senderaddr + 1
		tb_print(xpos, ypos, width, ypos, email.Mail_subject, fg, bg)
	}
}

// print_paged_text pretty-prints a string, respecting line breaks and
// adjusting it to screen width and height constraints. It can also
// optionally highlight fragments of it (for example URLs) with a
// different style. Starting line can be provided by
// map_key_to_scroll() for simple pager-like scrolling functionality.
func print_paged_text(ystart, ymax int, text string, linebreaks []int, start_line int, highlight_links bool, link_indices [][]int, selected_link int) {
	ypos := ystart
	var is_link, is_selected_link bool
	var fg, bg termbox.Attribute
	for line := start_line; ypos < ymax && line < len(linebreaks); line++ {
		xpos := 0
		line_start_offset := linebreaks[line]
		var next_line_offset int
		if len(linebreaks) > line+1 {
			next_line_offset = linebreaks[line+1]
		} else {
			next_line_offset = len(text)
		}
		// Print from this offset until just before the next (i.e. this line).
		for lindex, c := range text[line_start_offset:next_line_offset] {
			fg, bg = TEXTFG, TEXTBG
			// Is current character part of a link we should highlight?
			this_char_offset := line_start_offset + lindex
			for match_i, match_pair := range link_indices {
				if match_pair[0] <= this_char_offset {
					is_link = true
					if match_i == selected_link {
						is_selected_link = true
					}
					if match_pair[1] <= this_char_offset {
						is_link = false
						is_selected_link = false
					}
				}
			}
			if highlight_links {
				if is_link {
					if is_selected_link {
						bg = SELLINKBG
						fg = SELLINKFG
					} else {
						bg = LINKBG
						fg = LINKFG
					}
				}
			}
			termbox.SetCell(xpos, ypos, c, fg, bg)
			xpos += runewidth.RuneWidth(c)
		}
		ypos++
	}
}

// print_input_box draws a simple dialogue that requests the user to
// input a string. It does not handle receiving that string, only
// drawing the dialogue.
func print_input_box(xstart, ystart, boxwidth int, input_string string, domain_index int, label_title string, input_max_width int) {
	// Text label positions.
	xstart_title := xstart + (boxwidth-runewidth.StringWidth(label_title))/2
	xstart_enter := xstart + 2
	xstart_escape := xstart + boxwidth - runewidth.StringWidth(LABEL_ESCAPE) - 2
	xstart_input := xstart + 2
	// If the input_string is longer than the input box, print only its tail.
	visible_input_string := input_string
	for runewidth.StringWidth(visible_input_string) > input_max_width {
		visible_input_string = string([]rune(visible_input_string)[1:])
	}
	// Clear drawing area.
	for i := ystart; i < ystart+6; i++ {
		fill_line(xstart, i, xstart+boxwidth, TEXTFG, TEXTBG)
	}
	// Draw box top.
	termbox.SetCell(xstart, ystart, '+', TEXTFG, TEXTBG)
	for i := xstart + 1; i < xstart+boxwidth-1; i++ {
		termbox.SetCell(i, ystart, '-', TEXTFG, TEXTBG)
	}
	termbox.SetCell(xstart+boxwidth-1, ystart, '+', TEXTFG, TEXTBG)
	// Draw box sides.
	for i := ystart + 1; i < ystart+5; i++ {
		termbox.SetCell(xstart, i, '|', TEXTFG, TEXTBG)
		termbox.SetCell(xstart+boxwidth-1, i, '|', TEXTFG, TEXTBG)
	}
	// Draw box bottom.
	termbox.SetCell(xstart, ystart+5, '+', TEXTFG, TEXTBG)
	for i := xstart + 1; i < xstart+boxwidth-1; i++ {
		termbox.SetCell(i, ystart+5, '-', TEXTFG, TEXTBG)
	}
	termbox.SetCell(xstart+boxwidth-1, ystart+5, '+', TEXTFG, TEXTBG)
	// Draw text labels.
	tb_print(xstart_title, ystart+1, xstart+boxwidth, ystart+1, label_title, TEXTFG, TEXTBG)
	tb_print(xstart_enter, ystart+4, xstart+boxwidth, ystart+3, LABEL_ENTER, TEXTFG, TEXTBG)
	tb_print(xstart_escape, ystart+4, xstart+boxwidth, ystart+3, LABEL_ESCAPE, TEXTFG, TEXTBG)
	// Draw input box.
	fill_line(xstart_input, ystart+2, xstart_input+USERNAME_INPUT_WIDTH, INPUTBOXFG, INPUTBOXBG)
	tb_print(xstart_input, ystart+2, xstart_input+USERNAME_INPUT_WIDTH, ystart+2, visible_input_string, INPUTBOXFG, INPUTBOXBG)
	tb_print(xstart_input+USERNAME_INPUT_WIDTH+1, ystart+2, xstart+boxwidth-2, ystart+2, "@", TEXTFG, TEXTBG)
	termbox.SetCursor(xstart_input+runewidth.StringWidth(visible_input_string), ystart+2)
	// Draw domain name.
	tb_print(xstart_input, ystart+3, xstart+boxwidth-2, ystart+3, sharklaser.DomainNames[domain_index], TEXTFG, TEXTBG)
}

func main() {
	var err error
	var extend_latest_attempt int64

	// Termbox initialisation.
	if err = termbox.Init(); err != nil {
		panic(err)
	}
	defer termbox.Close()

	termbox.Clear(TEXTFG, TEXTBG)
	termbox.Sync()

	// shark := sharklaser.New()
	grrla := Grrla{
		Sharklaser: sharklaser.New(),
	}
	grrla.resize(termbox.Size())
	grrla.switch_mode(MODE_IDLE)
	grrla.options, err = parse_config_file()

	// Sharklaser initialisation.
	grrla.UserAgent = USERAGENT
	grrla.DomainName = grrla.options.domain_name
	if len(grrla.options.subscr_cookie) > 0 {
		grrla.SubsToken = grrla.options.subscr_cookie
	}

	// grrla.WriteDebugLog = true
	// grrla.DebugLogFname = "sharklaser.log"
	grrla.DummyMode = true
	grrla.DummyDir = "dummy"

	if grrla.options.auto_getaddr {
		grrla.set_error(grrla.SLGetAddr())
	}
	if grrla.err != nil {
		os.Stderr.WriteString(err.Error())
		os.Exit(1)
	}
	if grrla.AddrActive {
		grrla.switch_mode(MODE_INBOX)
		if len(grrla.options.email_user) > 0 {
			err = grrla.SLSetUser(grrla.options.email_user)
		}
		if grrla.options.get_mail_on_start {
			grrla.set_error(grrla.SLUpdate())
		} else {
			grrla.NextUpdate = time.Now().Unix() + grrla.UpdateInt
		}
	}

	// Prepare the channels that the main loop runs on.
	// Ticker to wake up every second and redraw the interface or do stuff.
	ticker := time.NewTicker(time.Second)
	defer ticker.Stop()
	// Keyboard event receiver.
	kbevchan := make(chan termbox.Event)
	go tb_getevent(kbevchan)

	// Start main loop.
	for {
		// Draw user interface.
		grrla.draw()

		// See what's up.
		select {

		case tick := <-ticker.C: // Wake up.

			if grrla.AddrActive {
				// Fetch new mail.
				if tick.Unix() >= grrla.NextUpdate {
					grrla.set_error(grrla.SLUpdate())
					grrla.set_status(STATUS_CHECK_EMAIL)
				}
				// Extend address lifetime.
				if grrla.ReckonTimeLeft() < EXTEND_THRESHOLD {
					if grrla.options.auto_extend && tick.Unix()-extend_latest_attempt > EXTEND_INTERVAL {
						grrla.set_error(grrla.SLExtend())
						grrla.set_status(STATUS_EXTEND)
						extend_latest_attempt = tick.Unix()
					}
				}
				// Renew expired address.
				if !grrla.AddrActive || grrla.ReckonTimeLeft() <= 0 {
					if grrla.options.auto_renew {
						if tick.Unix()-extend_latest_attempt > EXTEND_INTERVAL {
							grrla.set_error(grrla.SLRenew())
							grrla.set_status(STATUS_RENEW)
							extend_latest_attempt = tick.Unix()
						}
					} else {
						grrla.switch_mode(MODE_IDLE)
					}
				}
			}

		case ev := <-kbevchan: // React to keyboard events.

			// Bindings for all modes.
			if ev.Type == termbox.EventResize {
				termbox.Sync()
				// Recalculate UI proportions.
				grrla.resize(termbox.Size())
				// Recalculate size-dependent display variables.
				grrla.scroll_message_list(0, len(grrla.Emails))
				// Need to recalculate linebreaks if width changes.
				grrla.prepare_paging(grrla.paging_text)
			} else {

				// Bindings for idle mode.
				if grrla.mode == MODE_IDLE {
					switch ev.Ch {
					case 'q':
						grrla.quit()
					case 'g':
						grrla.set_error(grrla.SLGetAddr())
						if grrla.AddrActive {
							grrla.switch_mode(MODE_INBOX)
							if len(grrla.options.email_user) > 0 {
								err = grrla.SLSetUser(grrla.options.email_user)
							}
							if grrla.options.get_mail_on_start {
								grrla.set_error(grrla.SLUpdate())
							} else {
								grrla.NextUpdate = time.Now().Unix() + grrla.UpdateInt
							}
						}
					case '?':
						grrla.switch_mode(MODE_HELP)
					}

					// Bindings for inbox mode.
				} else if grrla.mode == MODE_INBOX {
					switch ev.Ch {
					case 'Q':
						grrla.quit()
					case 'c':
						grrla.set_error(command_with_string(grrla.EmailAddr, grrla.options.address_cmd))
						grrla.set_status(STATUS_CMD_ADDR)
					case 'r':
						grrla.set_error(grrla.SLUpdate())
						grrla.set_status(STATUS_CHECK_EMAIL)
					case '?':
						grrla.switch_mode(MODE_HELP)
					case 's':
						grrla.switch_mode(MODE_SETEMAIL)
					case 'd':
						grrla.Emails[grrla.msg_current].FlaggedForDeletion = !grrla.Emails[grrla.msg_current].FlaggedForDeletion
					case 'D':
						grrla.set_error(grrla.SLDelEmail())
						// Recalculate selected message to avoid selecting a deleted one.
						grrla.scroll_message_list(0, len(grrla.Emails))
						grrla.set_status("")
					case 'C':
						grrla.set_error(command_with_string(grrla.Alias, grrla.options.alias_cmd))
						grrla.set_status(STATUS_CMD_ALIAS)
					case 'F':
						grrla.set_error(grrla.SLForgetMe())
					}
					switch ev.Key {
					case termbox.KeyArrowUp, termbox.KeyArrowDown, termbox.KeyPgup, termbox.KeyPgdn, termbox.KeyHome, termbox.KeyEnd:
						// Scroll message list.
						if len(grrla.Emails) > 0 {
							amount := map_key_to_scroll(ev.Key, grrla.height_inbox_list, len(grrla.Emails))
							grrla.scroll_message_list(amount, len(grrla.Emails))
						}
					case termbox.KeyEnter:
						// Enter single message display mode.
						if len(grrla.Emails) > grrla.msg_current {
							// Fetch message body and calculate linebreaks.
							_, err = grrla.SLFetchEmail(grrla.msg_current)
							grrla.set_status("")
							grrla.switch_mode(MODE_MESSAGE)
						}
					}

					// Bindings for single message display mode.
				} else if grrla.mode == MODE_MESSAGE {
					switch ev.Ch {
					case 'q':
						grrla.switch_mode(MODE_INBOX)
					case 's':
						grrla.set_error(save_message(grrla.EmailAddr, grrla.Emails[grrla.msg_current]))
						grrla.set_status(STATUS_SAVE_MSG)
					case 'c':
						start := grrla.msg_link_indices[grrla.msg_selected_link][0]
						end := grrla.msg_link_indices[grrla.msg_selected_link][1]
						link_text := grrla.Emails[grrla.msg_current].Mail_body[start:end]
						grrla.set_error(command_with_string(link_text, grrla.options.link_cmd))
						grrla.set_status(STATUS_CMD_URL)
					case '?':
						grrla.switch_mode(MODE_HELP)
					case 'd':
						grrla.Emails[grrla.msg_current].FlaggedForDeletion = !grrla.Emails[grrla.msg_current].FlaggedForDeletion
					}
					switch ev.Key {
					case termbox.KeyArrowUp, termbox.KeyArrowDown, termbox.KeyPgup, termbox.KeyPgdn, termbox.KeyHome, termbox.KeyEnd:
						// Scroll message display.
						grrla.scroll(ev.Key)
					case termbox.KeyTab:
						// Change selected link.
						grrla.msg_selected_link++
						if grrla.msg_selected_link == len(grrla.msg_link_indices) {
							grrla.msg_selected_link = 0
						}
					}

					// Bindings for username input mode.
				} else if grrla.mode == MODE_SETEMAIL {
					// For normal key presses, add their character to the username.
					// Accept only printable characters.
					if matched, _ := regexp.MatchString(REGEXP_PRINTABLE, string(ev.Ch)); matched {
						grrla.setusr_username += string(ev.Ch)
					}
					switch ev.Key {
					case termbox.KeyEnter:
						// Submit.
						grrla.DomainName = sharklaser.DomainNames[grrla.setusr_domain_index]
						grrla.set_error(grrla.SLSetUser(grrla.setusr_username))
						if grrla.err == nil {
							grrla.setusr_username = ""
						}
						grrla.set_status(STATUS_SETUSR)
						grrla.switch_mode(MODE_INBOX)
					case termbox.KeyEsc:
						// Cancel.
						grrla.setusr_username = ""
						grrla.switch_mode(MODE_INBOX)
					case termbox.KeyTab:
						grrla.setusr_domain_index++
						if grrla.setusr_domain_index == len(sharklaser.DomainNames) {
							grrla.setusr_domain_index = 0
						}
					case termbox.KeyBackspace, termbox.KeyBackspace2:
						// Delete last character.
						newlen := len([]rune(grrla.setusr_username)) - 1
						if newlen < 0 {
							newlen = 0
						}
						grrla.setusr_username = string([]rune(grrla.setusr_username)[:newlen])
					}

					// Bindings for help mode.
				} else if grrla.mode == MODE_HELP {
					switch ev.Ch {
					case 'q':
						grrla.switch_mode(grrla.previous_mode)
					}
					switch ev.Key {
					case termbox.KeyArrowUp, termbox.KeyArrowDown, termbox.KeyPgup, termbox.KeyPgdn, termbox.KeyHome, termbox.KeyEnd:
						// Scroll message display.
						grrla.scroll(ev.Key)
					}
				} // else if grrla.mode == MODE_HELP
			} // case ev := <-kbevchan
		} // select
	} // for
} // main()
